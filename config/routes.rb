Rails.application.routes.draw do
  resources :assign_employees
  get 'dashboard/index'
  get 'assign_employees/index'

  get 'employees/list'

  get 'clients/list'

  root 'login_page#login'
  get 'application/hello'
end
