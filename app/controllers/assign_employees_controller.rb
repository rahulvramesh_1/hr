class AssignEmployeesController < ApplicationController
  before_action :set_assign_employee, only: [:show, :edit, :update, :destroy]

  # GET /assign_employees
  # GET /assign_employees.json
  def index
    @assign_employees = AssignEmployee.all
  end

  # GET /assign_employees/1
  # GET /assign_employees/1.json
  def show
  end

  # GET /assign_employees/new
  def new
    @assign_employee = AssignEmployee.new
  end

  # GET /assign_employees/1/edit
  def edit
  end

  # POST /assign_employees
  # POST /assign_employees.json
  def create
    @assign_employee = AssignEmployee.new(assign_employee_params)

    respond_to do |format|
      if @assign_employee.save
        format.html { redirect_to @assign_employee, notice: 'Assign employee was successfully created.' }
        format.json { render :show, status: :created, location: @assign_employee }
      else
        format.html { render :new }
        format.json { render json: @assign_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assign_employees/1
  # PATCH/PUT /assign_employees/1.json
  def update
    respond_to do |format|
      if @assign_employee.update(assign_employee_params)
        format.html { redirect_to @assign_employee, notice: 'Assign employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @assign_employee }
      else
        format.html { render :edit }
        format.json { render json: @assign_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assign_employees/1
  # DELETE /assign_employees/1.json
  def destroy
    @assign_employee.destroy
    respond_to do |format|
      format.html { redirect_to assign_employees_url, notice: 'Assign employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assign_employee
      @assign_employee = AssignEmployee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assign_employee_params
      params.require(:assign_employee).permit(:client, :employee, :assign_date, :comments)
    end
end
